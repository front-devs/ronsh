<?php


/**
 * RouteLoader
 */
class RouteLoader {

  /**
   * @var Route $routes
   */
  private $routes = [];


  /**
   * @param string $file
   */
  function __construct($file)
  {

    // read json
    $json = file_get_contents($file);

    // parse to array
    $json = json_decode($json, true);

    // final parse
    $this->parseJsonToRoutes($json);

  }

  /**
   * @param Route $route
   * @return RouteLoader
   */
  public function addRoute($route)
  {
    // add the route to routes array
    array_push($this->routes, $route);
    // return me
    return $this;
  }

  /**
   * @param string $route
   * @param string $method
   * @param string $controller
   * @param string $function
   * @return RouteLoader
   */
   public function addRouteRaw($route, $method, $controller, $function)
   {
     // create a route
     $route = new Route($route, $method, $controller, $function);
     // add the Route
     return addRoute($route);
   }

  /**
   * checks all routes it has and returns a matching route if one exists
   * @param string $url
   * @param string $method
   * @return string
   */
  public function getRoute($url, $method)
  {
    // loop through all routes
    foreach ($this->routes as $route) {
      // check if route is the right one
      if ($route->check($url, $method)) {
        // return this one
        return $route;
      }
    }
    // return nothing
    return null;
  }

  /**
   * @param array $json
   * @param string $group = ""
   * @param string $controller = null
   */
  private function parseJsonToRoutes($json, $group = "", $controller = null)
  {
    // $json needs to be an array
    if (is_array($json)) {

      // set controller class if set
      if (array_key_exists('controller', $json)) {
        $controller = $json['controller'];
      }

      // set a route if set
      if (array_key_exists('page', $json)) {
        // if no controller is set throw exception
        if (null === $controller) {
          throw new RouteException(RouteException::NOT_DEFINED_CONTROLLER);
        }
        // add route from 'page' entry
        $page = $json['page'];
        // put page into an array to also manage an array
        if (array_key_exists('function', $page)) {
          $page = [$page];
        }
        // loop through the page entries (there are maybe multiple methods)
        foreach ($page as $p) {
          $this->addRoute(new Route($group, $p['method'], $controller, $p['function']));
        }
      }

      // loop through all entries and call this function recursively
      foreach ($json as $route=>$routes) {
        // check if $routes is an array because it need to be that
        if (is_array($routes)) {
          $this->parseJsonToRoutes($routes, $group.$route, $controller);
        }
      }

    }
    
    // return me
    return $this;
  }

}
