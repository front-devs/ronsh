<?php

/**
 * Route
 */
class Route {

  /**
   * @var string $route
   */
  private $route;
  /**
   * @var string $method
   */
  private $method;
  /**
   * @var string $controller
   */
  private $controller;
  /**
   * @var string $function
   */
  private $function;

  /**
   * @param string $route
   * @param string $method
   * @param string $controller
   * @param string $function
   */
  public function __construct($route, $method, $controller, $function)
  {
    // initialize variables
    $this->route = $route;
    $this->method = strtoupper($method);
    $this->controller = $controller;
    $this->function = $function;
  }

  /**
   * @param string $url
   *
   * @return boolean
   */
  public function check($url, $method)
  {
    // upper the method for better check result
    $method = strtoupper($method);

    // check if it is the same method
    $isMyMethod = false;
    $myMethod = $this->getMethod();
    // if is an array check all my method entries with requesting $method
    if (is_array($myMethod)) {
      // loop throw methods
      foreach ($myMethod as $m) {
        if ($m == $method) {
          $isMyMethod = true;
          break;
        }
      }
    } else {
      // if I have only one method
      // if all methods should be allowed
      if ($myMethod == '*' || $myMethod == 'all') {
        $isMyMethod = true;
      } else {
        // give $isMyMethod the boolean if methods are equally
        $isMyMethod = ($myMethod == $method);
      }
    }

    // if it's not this method of the route the route is the wrong one
    if (!$isMyMethod) {
      return false;
    }

    // standarisize
    $url = UrlBeautifuller::standarisize($url);

    // check if $url equals
    if ($url == $this->route) {
      return true;
    }
    
    // return false otherwise
    return false;
  }

  /**
   * @return string
   */
  public function getRoute()
  {
    return $this->route;
  }

  /**
   * @return string
   */
  public function getMethod()
  {
    return $this->method;
  }

  /**
   * @return string
   */
  public function getController()
  {
    return $this->controller;
  }

  /**
   * @return string
   */
  public function getFunction()
  {
    return $this->function;
  }

}
