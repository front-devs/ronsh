<?php


/**
 * class UrlBeautifuller
 */
class UrlBeautifuller
{

  /**
   * @param string $url
   * @return string
   */
  public static function standarisize($url)
  {
    // lower for case desesitve
    $url = strtolower($url);
    // removing ?a=b&c=d...
    $pos = strpos($url, '?');
    if (false !== $pos) {
      $url = substr($url, 0, $pos);
    }
    // removing trailing slash
    if ($url{strlen($url)-1} == '/') {
      $url = substr($url, 0, -1);
    }
    //return it
    return $url;
  }

}
