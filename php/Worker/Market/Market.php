<?php

/* no need at the moment */
/* wasn't revised */

/**
 * class Market
 */
class Market {

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
    * @var string
    */
    private $description;


    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
     public function getName()
     {
         return $this->name;
     }

     /**
     * @return string
     */
     public function getDescription()
     {
         return $this->description;
     }

}