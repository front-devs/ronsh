<?php

/*
 * class MarketService
 */
class MarketService {

  /**
   * @var MarketRepository
   */
  private $marketRepository;


  /* Init */
  public function __construct() {
    $this->marketRepository = new MarketRepository();
  }

  /**
   * @param int $id
   * @return array|null
   */
  public function getMarket($id)
  {
    // get the market
    $market = $this->marketRepository->getMarket($id);
    // if market wasn't found return nothing
    if (!$market) {
      return null;
    }
    // return the first (there is only one) entry
    $market = $market[0];
    return $market;
  }

  /**
   * @param array $ids
   * @return array|null
   */
   public function getMarkets(array $ids)
   {
     // we need to prepare the query with many OR conditions
     $preparedQuery = "";
     $variables = [];
     if (count($ids)) {
      if ($ids[0] == 0) {
        // do nothing because we don't filter the things (0 = all)
      } else {
        // set $first to true so that the first time no OR is set because there wasn't another condition
        $first = true;
        // loop through all $ids and fill the string $preparedQuery
        foreach ($ids as $id) {
          if (!$first) {
            $preparedQuery .= " OR ";
          } else {
            $first = false;
          }
          $preparedQuery .= "WHERE id=?";
          array_push($variables, intval($id));
        }
      }
      // get branches with the query
      $result = $this->marketRepository->getMarkets($preparedQuery, $variables);
      // return the branches
      return $result;
    }

    return null;
   }

}
