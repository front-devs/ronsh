<?php

/**
 * class CurrencyService
 */
class CurrencyService {

  /**
   * @param int $cents
   * @return string
   */
  public static function toEuro($cents)
  {
    // convert integer to string
    $str = strval($cents);
    // if string needs , because it's under 1€
    if ($cents < 100) {
      if ($cents < 10) {
        // if it's under 10 cents insert a 0,0 before the value
        $str = '0,0'.$str;
      } else {
        // else it's only under 100 and insert only a 0, before
        $str = '0,'.$str;
      }
    } else {
      // if it's min. a 1€ insert a , at the second last position
      $str = substr_replace($str, ',', -2, 0);
    }
    // finally add a € and return it
    return $str.'€';
  }

}
