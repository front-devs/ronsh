<?php

/*
 * class ProductService
 */
class ProductService {

  /**
   * @var ProductRepository
   */
  private $productRepository;
  /**
   * @var MarketService
   */
  private $marketService;
  /**
   * @var BranchService
   */
  private $branchService;


  /* Init */
  public function __construct() {
    // initialize repository
    $this->productRepository = new ProductRepository();
    // initialize services
    $this->marketService = new MarketService();
    $this->branchService = new BranchService();
  }

  /**
   * @param string $query
   * @return array
   */
  public function search($query)
  {
    // remove %'s
    $query = str_replace('%', '', $query);
    // change  *'s to %'s for better search quality
    $query = str_replace('*', '%', $query);
    // search the products with the repository
    $result = $this->productRepository->search($query);
    // parse result
    $len = count($result);
    // todo: need to use a Product-class with serialize and deserialize
    for ($i = 0; $i < $len; ++$i) {
      $result[$i]['priceInEuro'] = CurrencyService::toEuro($result[$i]['price']);
      $result[$i]['image'] = dechex($result[$i]['image']);
      $result[$i]['market'] = null;
      $result[$i]['branch'] = null;
    }
    return $result;
  }

  /**
   * @param int $id
   * @return array
   */
  public function getProduct($id)
  {
    // get product from repository
    $result = $this->productRepository->getProduct($id);
    // parse result
    if (!$result) {
      return null;
    }
    // also need to use a ProductClass
    $result = $result[0];
    $result['priceInEuro'] = CurrencyService::toEuro($result['price']);
    $result['image'] = dechex($result ['image']);
    $result['market'] = $this->marketService->getMarket($result['market']);
    $result['branch'] = $this->branchService->getBranch($result['branch']);
    return $result;
  }

  /**
   * @param int $market
   * @param int $branch
   * @param array $product
   * @return ProductService
   */
  public function addProduct($market, $branch, array $product, $image_index)
  {
    // add the product with the repository
    $this->productRepository->addProduct($market, $branch, $product['name'], $product['description'], $product['price'], $image_index);
    // return myself
    return $this;
  }

}
