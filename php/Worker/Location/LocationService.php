<?php

/* This wasn't revised because we don't need it here*/

class LocationService {

  private $cookieService;
  private $setLocation = false;
  private $location = [
    'latitude' => null,
    'longitude' => null,
    'country' => 'germany',
    'city' => 'steinfurt-borghorst',
  ];

  public function __construct() {
    $this -> cookieService = new CookieService();
    if ($this->cookieService->cookiesEnabled())
      $this->loadLocation();
  }

  public function loadLocation() {
    $location = $this->cookieService.get('location', null);
    if ($location != null) {
      $location = json_decode($location);
      if ((array_key_exists('country',$location) && array_key_exists('city',$location)) || (array_key_exists('latitude',$location) && array_key_exists('longitude',$location))) {
        $this->locationIsSet = true;
        foreach ($location as $key=>$thing) {
          $this->location[$key] = $thing;
        }
        return true;
      }
    }
    return false;
  }

  public function saveLocation() {
    $this->cookieService.set('location', json_encode($this->location));
  }

  public function getLocation() {
    return $this->location;
  }

  public function getLocationSecure() {
    $location = $this->location;
    foreach ($location as $key=>$thing) {
      $location[$key] = rawurlencode($thing);
    }
    return $location;
  }

  public function setLocation($latitude, $longitude, $country, $city) {
    $this->location = [
      'latitude' => $latitude,
      'longitude' => $longitude,
      'country' => $country,
      'city' => $city
    ];
  }

  public function setLocationLatitude($latitude) {
    $this->location['latitude'] = $latitude;
  }

  public function setLocationLongitude($latitude) {
    $this->location['longitude'] = $latitude;
  }

  public function setLocationCountry($latitude) {
    $this->location['country'] = $latitude;
  }

  public function setLocationCity($latitude) {
    $this->location['city'] = $latitude;
  }

  public function locationIsSet() {
    return $this->setLocation;
  }

}
