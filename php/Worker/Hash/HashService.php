<?php


/**
 * class HashService
 */
class HashService
{

  /**
   * @param int $hardness = 22
   * @return string
   */
  public static function generateRandomHash($hardness = 22) {
    return bin2hex(random_bytes($hardness));
  }

  /**
   * @param string $str
   * @return string
   */
  public static function simpleHash($str) {
    return md5($str);
  }

  /**
   * @param string $username
   * @param string $password
   * @return string
   */
  public static function generateToken($username, $password) {
    return crypt(crypt($username, $password), self::generateRandomHash());
  }

  /**
   * @param string $str
   * @return string
   */
  public static function hashPassword($password) {
    return password_hash($password, PASSWORD_BCRYPT);
  }

  /**
   * @param string $password
   * @param string $hash
   * @return boolean
   */
  public static function checkPassword($password, $hash) {
    return password_verify($password, $hash);
  }

}
