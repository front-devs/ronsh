<?php

/*
 * class BranchService
 */
class BranchService {

  /**
   * @var MarketRepository
   */
  private $branchRepository;


  /* Init */
  public function __construct() {
    // initialize repository
    $this->branchRepository = new BranchRepository();
  }

  /**
   * @param int $id
   * @return array|null
   */
  public function getBranch($id)
  {
    // get the branch
    $branch = $this->branchRepository->getBranch($id);
    // if branch not exists return nothing
    if (!$branch) {
      return null;
    }
    // get the first branch (there is only one)
    $branch = $branch[0];
    // return it
    return $branch;
  }

  /**
   * @param array $ids
   * @return array|null
   */
   public function getBranches(array $ids)
   {
     // we need to prepare the query with many OR conditions and that ids
     $preparedQuery = "";
     $variables = [];
     if (count($ids)) {
      if ($ids[0] == 0) {
        // do nothing because we don't filter the things (0 = all)
      } else {
        // set $first to true so that the first time no OR is set because there wasn't another condition
        $first = true;
        // loop through all $ids and fill the string $preparedQuery and add the id to the variables
        foreach ($ids as $id) {
          if (!$first) {
            $preparedQuery .= " OR ";
          } else {
            $first = false;
          }
          $preparedQuery .= "WHERE id=?";
          array_push($variables, intval($id));
        }
      }
      // get branches with the query
      $result = $this->branchRepository->getBranches($preparedQuery, $variables);
      // return the branches
      return $result;
    }
   }

}
