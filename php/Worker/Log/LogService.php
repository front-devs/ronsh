<?php

/**
 * class LogService
 */
class LogService
{

    private static $alreadyLogged = false;
  
    private static function log($file, $message)
    {
        // prepare message
        $message = "\n$message\n";
        if (!self::$alreadyLogged) {
            // add additional information to this request one time
            $message ="\n\n[" . date("d.m.Y, H:s") . "] ---- (" . $_SERVER['REQUEST_URI'] . ") ----------\n" . $message;
            self::$alreadyLogged = true;
        }
        // set filenames
        $seperatedLogFile = $file;
        $logFile = "log.log";
        // get log dir
        $logDir = ConfigService::getRootDir()."/log/";

        // write to seperated log file
        $h = fopen($logDir.$seperatedLogFile, "a");
        fwrite($h, $message);
        fclose($h);
        // write to log file
        $h = fopen($logDir.$logFile, "a");
        fwrite($h, $message);
        fclose($h);

        // log to the console
        error_log("$message");
    }

    public static function info($message)
    {
        self::log("infos.log", "[INFO] $message");
    }

    public static function warning($message)
    {
        self::log("warnings.log", "[WARNING] $message");
    }

    public static function error($message)
    {
        self::log("errors.log", "[ERROR] $message");
    }

    public static function unimportantError($message)
    {
        self::log("unimportantErrors.log", "[UNIMPORTANT ERROR] $message");
    }

}
