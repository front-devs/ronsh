<?php

/**
 * class CookieService
 */
class CookieService {

  /**
   * @var boolean
   */
  public $cookies_enabled = false;
  /**
   * @var int
   */
  public $expire_default = 60*60*24*356;

  /* Init */
  public function __construct() {
    // checks if there was set that cookies were enabled
    $this->cookies_enabled = array_key_exists('cookies_enabled', $_COOKIE) && $_COOKIE['cookies_enabled'];
  }

  /**
   * @return boolean
   */
  public function cookiesEnabled() {
    return $this->cookies_enabled;
  }

  /**
   * @param string $name
   * @param string $val
   * @param int $expire = null
   */
  public function setCookie($name, $val, $expire = null) {
    // only set the cookie if cookies are enabled
    if ($cookies_enabled) {
      // if expire not set set it to default
      if (null === $expire) {
        $expire = time() + $this->expire_default;
      }
      // set the cookie
      setcookie($name, $val, $expire, '/');
    }
  }

  /**
   * @param string $name
   * @param string $default = null
   */
  public function getCookie($name, $default = null) {
    // check if cookies are enabled and if this cookie with the $name exists
    if ($this->cookies_enabled && array_key_exists($name, $_COOKIE)) {
      // return the cookie
      return $_COOKIE[$name];
    }
    // return default if no cookie is found
    return $default;
  }

  /**
   * @param int $expire
   */
  public function setExpire($expire) {
    $this->expire_default = $expire;
  }
}
