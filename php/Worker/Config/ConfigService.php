<?php

/**
 * class ConfigService
 */
class ConfigService
{

    /**
     * @return string
     */
    public static function getResDir()
    {
        // get the root path
        $path = self::getRootDir();
        // go from .../ronsh to .../ronsh/php/res
        $path .= '/php/res';
        // return path
        return $path;
    }

    /**
     * @return string
     */
     public static function getRootDir()
     {
        // get the path of this file ("var/www/html/ronsh/php/Worker/Config")
        $path = __DIR__;
        // loop to .../ronsh (dirname($path, 3) would do the same, but this wont work for the test server)
        for ($i = 0; $i < 3; ++$i) {
            $path = dirname($path);
        }
        // return the path
        return $path;
     }

    /**
     * @return array
     */
    public static function getConfig()
    {
        return self::load('config.json');
    }

    /**
     * @return array
     */
    public static function getLoginConfig()
    {
        return self::load('loginConfig.json');
    }

    /**
     * will load the file with name $filename out of the .../res/config/* dir
     * @param string $filename
     * @return array
     */
    public static function load($filename)
    {
        return json_decode(file_get_contents(self::getResDir().'/config/'.$filename), true);
    }

    /**
     * @param array $config
     */
     public static function setConfig($config)
     {
         return self::write('config.json', $config);
     }
 
     /**
      * @param array $config
      */
     public static function setLoginConfig($config)
     {
         return self::write('loginConfig.json', $config);
     }
 
     /**
      * will write the file with name $filename with content $json to the .../res/config/* dir
      * @param string $filename
      * @param array $json
      */
     public static function write(string $filename, array $json)
     {
         file_put_contents(self::getResDir().'/config/'.$filename, json_encode($json));
     }

}