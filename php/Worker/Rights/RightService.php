<?php

/*
 * class RightService
 */
class RightService
{

  /**
   * @var RightRepository
   */
  private $rightRepository;

  /**
   * constructor
   */
  public function __construct()
  {
    $this->rightRepository = new RightRepository();
  }

  /**
   * @param Right $right
   * @param Account $account
   */
  public function addRight(Right $right, Account $account)
  {
    $this->rightRepository->addRight($right->getType(), $right->getValue(), $account->getId());
  }

  /**
   * @param Account $account
   * @param Rights $rights
   * @return array
   */
  public function getRights($account, $rights = null)
  {
    // deserialize array from repository
    return Rights::deserialize($this->rightRepository->getRights($account->getId()), $rights);
  }

}
