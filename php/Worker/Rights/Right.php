<?php


/**
 * class Right
 */
class Right
{
  /**
   * @var int
   */
  private $type;
  /**
   * @var int
   */
  private $value;

  /**
   * constructor
   */
  public function __construct($type, $value)
  {
    // setup variables
    $this->type  = $type;
    $this->value = $value;
  }


  /**
   * @return int
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * @return int
   */
  public function getValue()
  {
    return $this->value;
  }


  /**
   * @return array
   */
  public function toArray()
  {
    return [
      'type' => $this->getType(),
      'value' => $this->getValue()
    ];
  }

  /**
   * @return array
   */
  public function serialize()
  {
    return $this->toArray();
  }

  /**
   * @param array $array
   * @return Right
   */
  public static function deserialize($array) {
    if (array_key_exists('type', $array)
    &&  array_key_exists('value', $array)) {
      return new self($array['type'], $array['value']);
    } else {
      throw new WebException("parse error while deserializing a Right");
    }
  }

}
