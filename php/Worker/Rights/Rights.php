<?php


/**
 * class Rights
 */
class Rights
{
  /**
   * @var array
   */
  private $rights;
  /**
   * @var RightService
   */
  private $rightService;
  /**
   * @var Account
   */
  private $account;

  /**
   * constructor
   */
  public function __construct($rights = [], Account $account = null)
  {
    // setup variables
    $this->rights = $rights;
    $this->account = $account;
    // initialize services
    $this->rightService = new RightService();
  }


  /**
   * @param Right $right
   * @return Rights
   */
  public function add($right, $update=true)
  {
    // add the $right
    array_push($this->rights, $right);
    // if it should be updated, the account was defined and exists
    if ($update && $this->account && $this->account->exists()) {
      // add the right so that it will be updated at the database
      $this->rightService->addRight($right, $this->account);
    }
    // return me
    return $this;
  }

  /**
   * @param Account $account
   * @return Rights
   */
  public function load($account)
  {
    // get rights from service
    $this->rightService->getRights($account, $this);
    return $this;
  }


  /**
   * @param int $type
   */
  public function getState($type)
  {
    foreach ($this->rights as $right) {
      if ($right->getType() == $type) {
        return $right->getValue();
      }
    }
    return null;
  }

  /**
   * @param int $type
   * @return array
   */
  public function getStates($type)
  {
    $states = [];
    foreach ($this->rights as $right) {
      if ($right->getType() == $type) {
        array_push($states, $right->getValue());
      }
    }
    return $states;
  }

  /**
   * @param int $type
   * @param int $value
   * @param boolean
   */
  public function checkState($type, $value)
  {
    foreach ($this->rights as $right) {
      if ($right->getType() == $type && $right->getValue() == $value) {
        return true;
      }
    }
    return false;
  }


  /**string $username
   * @param string $hash
   * @return array
   */
  public function toArray()
  {
    $a = [];
    foreach ($this->rights as $right) {
      array_push($a, $right->serialize());
    }
    return $a;
  }

  /**
   * @return array
   */
  public function serialize()
  {
    return $this->toArray();
  }

  /**
   * @param array $array
   * @return Right
   */
  public static function deserialize($array, $rights = null) {
    if (!$rights) {
      $rights = new self();
    }
    foreach ($array as $right) {
      $rights->add(Right::deserialize($right), false);
    }
    return $rights;
  }

}
