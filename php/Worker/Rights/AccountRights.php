<?php


/**
 * class AccountRights
 */
class AccountRights extends Rights
{

  /* defining rights */
  const RIGHT_READ  = 0;
  const RIGHT_WRITE = 1;
  const RIGHT_CREATE_ACCOUNT = 2;
  const RIGHT_REMOVE_ACCOUNT = 3;
  const RIGHT_CHANGE_ACCOUNT_RIGHT = 4;
  const RIGHT_CREATE_MARKET = 5;
  const RIGHT_CREATE_BRANCH = 6;
  const RIGHT_WRITE_MARKET = 7;
  const RIGHT_WRITE_BRANCH = 8;
  

  /**
   * constructor
   */
  public function __construct()
  {
    parent::__construct($rights);
  }

}
