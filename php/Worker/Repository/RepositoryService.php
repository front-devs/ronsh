<?php


class RepositoryService {

  /**
   * @var PDO
   */
  private $connection;
  /**
   * @var string
   */
  private $host;
  /**
   * @var string
   */
  private $username;
  /**
   * @var string
   */
  private $password;
  /**
   * @var string
   */
  private $database;

  public function __construct() {
    try {
      // loading login data
      $login = ConfigService::getLoginConfig();
      $login = $login['database'];
      $this->host     = $login['host'];
      $this->database = $login['dbname'];
      $this->username = $login['username'];
      $this->password = $login['password'];
      // logging in
      $this->connection = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->database .';charset=utf8', $this->username, $this->password);
      // setup settings
      $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    } catch (PDOException $pdoe) {
      // throw error while connecting
      $this->error($pdoe->getMessage(), 'while connecting');
    }
  }

  /**
   * @param string $sql
   * @return PDOStatement
   */
  public function run(string $sql, array $variables = []) {
    try {
      // prepare sql
      $sth = $this->connection->prepare($sql);
      // iterate through all variables and bind that
      $len = count($variables);
      for ($i = 0; $i < $len; ++$i) {
        $sth->bindParam($i+1, $variables[$i]);
      }
      // run sql
      $sth->execute();
      // return so that you can get a json of that
      return $sth;
    } catch(PDOException $pdoe) {
      // on error throw it
      $this->error($pdoe->getMessage(), $sql);
    }
    return null;
  }

  /**
   * @param string $sql
   * @return array
   */
  public function get(string $sql, array $variables = [])
  {
    try {
      return $this->getFromPDOStatement($this->run($sql, $variables));
    } catch(PDOException $pdoe) {
      $this->error($pdoe->getMessage(), $sql);
    }
  }

  /**
   * @param PDOStatement $sth
   * @return array
   */
  public function getFromPDOStatement(PDOStatement $sth)
  {
    try {
      // convert result to json
      $json = $sth->fetchAll(PDO::FETCH_ASSOC);
      // decode encoded strings
      foreach ($json as $index=>$entry) {
        foreach ($entry as $key=>$value) {
          $json[$index][$key] = $value;
        }
      }
      // return the json
      return $json;
    } catch(PDOException $pdoe) {
      $this->error($pdoe->getMessage(), $sql);
    }
  }

  /**
   * Will throw RepositoryException
   * @param $e
   */
  private function error($e, $sql)
  {
    throw new RepositoryException(RepositoryException::PDO_ERROR, $e.': '.$sql);
  }

}
