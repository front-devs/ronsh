<?php


/**
 * class BranchRepository
 */
class BranchRepository
{

  /**
   * @var Repository $repository
   */
  private $repository;


  /* Init */
  public function __construct()
  {
    $this->repository = new RepositoryService();
  }

  /**
   * @param int $id
   * @return array
   */
  public function getBranch($id)
  {
    // get whole branch with $id
    return $this->repository->get("SELECT * FROM branches WHERE id=?", [$id]);
  }

  /**
   * @param string $preparedQuery
   * @param array $variables
   * @return array
   */
   public function getBranches(string $preparedQuery, array $ids)
   {
     // return all branches with conditions $preparedQuery
     return $this->repository->get("SELECT * FROM branches $preparedQuery", $ids);
   }

}
