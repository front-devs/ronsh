<?php


/**
 * class ProductRepository
 */
class ProductRepository
{

  /**
   * @var Repository $repository
   */
  private $repository;


  /* Init */
  public function __construct()
  {
    // initialize RepositoryService
    $this->repository = new RepositoryService();
  }

  /**
   * @param string $query
   * @return array
   */
  public function search($query)
  {
    // get all products with name Like '%$query%'
    return $this->repository->get("SELECT * FROM products WHERE name LIKE ?", ["%$query%"]);
  }

  /**
   * @param int $id
   * @return array
   */
  public function getProduct($id) {
    // return whole product with $id
    return $this->repository->get("SELECT * FROM products WHERE id=?", [$id]);
  }

  /**
   * @param int $market
   * @param int $branch
   * @param string $name
   * @param string $description
   * @param int $price
   * @return ProductRepository
   */
   public function addProduct($market, $branch, $name, $description, $price, $image_index) {
    // converting variables to numbers
    $market += 0;
    $branch += 0;
    $price += 0;
    $image_index += 0;
    // validate values
    if ($market > 0 && $branch > 0 && $price > 0 && $image_index > 0) {
      // inserting the product
      $this->repository->set("INSERT INTO products (market, branch, name, description, price, image) VALUES (?, ?, ?, ?, ?, ?)", [$market, $branch, $name, $description, $price, $image_index]);
      // returning myself
      return $this;
    }
    throw new WebException("addProduct: values are wrong (not a number or <= 0)", 500);
    return false;
  }

}
