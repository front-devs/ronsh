<?php

/**
 * class AccountRepository
 */
class AccountRepository
{

  /**
   * @var Repository
   */
  private $repository;


  /* Init */
  public function __construct()
  {
    // initialize RepositoryService
    $this->repository = new RepositoryService();
  }

  /**
   * @param string $username
   * @param string $passwordHash
   * @return AccountRepository
   */
  public function addAccount($username, $passwordHash)
  {
    // secure values
    $username = $this->repository->secure($username);
    // insert the account
    $this->repository->set("INSERT INTO accounts (username, passwordHash) VALUES ('$username', '$passwordHash');");
    return $this;
  }

  /**
   * @param string $username
   * @return Account
   */
  public function getAccount($username)
  {
    // secure values
    $username = $this->repository->secure($username);
    // get whole account with $username
    return $this->repository->get("SELECT * FROM accounts WHERE username='$username'");
  }

  /**
   * @param string $token
   * @return array
   */
  public function getAccountFromToken($token)
  {
    // secure values
    $token = $this->repository->secure($token);
    // get whole account with $token
    return $this->repository->get("SELECT * FROM accounts WHERE token='$token'");
  }

  /**
   * @param int $id
   * @param string $username
   * @param string $passwordHash
   * @param string $token
   * @param int $timeOfToken
   * @param int $timeout
   * @return AccountRepository
   */
  public function update($id, $username, $passwordHash, $token, $timeOfToken, $timeout)
  {
    // secure values and setup times
    $timeOfToken = date('Y-m-d H:i:s', intval($timeOfToken));
    $timeout     = date('Y-m-d H:i:s', intval($timeout));
    // update database
    $this->repository->run(
      "UPDATE accounts SET
        username = ?,
        passwordHash = ?,
        token = ?,
        timeOfToken = ?,
        timeout = ?
      WHERE id = ?;", [
        $username,
        $passwordHash,
        $token,
        $timeOfToken,
        $timeout,
        $id
      ]);
    return $this;
  }



}
