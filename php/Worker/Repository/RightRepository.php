<?php


/**
 * class RightRepository
 */
class RightRepository
{

  /**
   * @var Repository
   */
  private $repository;


  /* Init */
  public function __construct()
  {
    // initialize RepositoryService
    $this->repository = new RepositoryService();
  }

  /**
   * @param int $right_type
   * @param int $right_value
   * @param int $account_id
   * @return RightRepository
   */
  public function addRight($right_type, $right_value, $account_id)
  {
    // insert the right
    $this->repository->run("INSERT INTO account_rights (account, type, value) VALUES (?,?,?)", [$account_id, $right_type, $right_value]);
    // return myself
    return $this;
  }

  /**
   * @param int $id
   * @return array
   */
  public function getRights($id)
  {
    // return whole rights with $id
    return $this->repository->get("SELECT * FROM account_rights WHERE account=?", [$id]);
  }

}
