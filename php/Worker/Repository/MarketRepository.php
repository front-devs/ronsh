<?php


/**
 * class MarketRepository
 */
class MarketRepository
{

  /**
   * @var Repository $repository
   */
  private $repository;


  /* Init */
  public function __construct()
  {
    // initialize RepositoryService
    $this->repository = new RepositoryService();
  }

  /**
   * @param int $id
   * @return array
   */
  public function getMarket($id)
  {
    // return whole market with $id
    return $this->repository->get("SELECT * FROM markets WHERE id=?", [$id]);
  }

  /**
   * @param string $preparedQuery
   * @param array $variables
   * @return array
   */
   public function getMarkets(string $preparedQuery, array $ids)
   {
     // return all markets with conditions $preparedQuery
     return $this->repository->get("SELECT * FROM markets $preparedQuery", $ids);
   }

}
