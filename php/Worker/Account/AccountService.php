<?php


/**
 * class AccountService
 */
class AccountService
{

  /**
   * @var AccountRepository
   */
  private $accountRepository;

  /* Init */
  public function __construct()
  {
    // initialize repository
    $this->accountRepository = new AccountRepository();
  }

  /**
   * @param Account $account
   * @return AccountRepository
   */
  public function addAccount($account)
  {
    $this->accountRepository->addAccount($account->getUsername(), $account->getPasswordHash());
    return $this;
  }

  /**
   * @param Account $account
   * @return AccountRepository
   */
  public function getAccount($username)
  {
    $account = $this->getAccountRaw($username);
    return Account::deserialize($account);
  }

  /**
   * @param Account $account
   * @return array
   */
  public function getAccountRaw($username)
  {
    $account = $this->accountRepository->getAccount($username);
    if (!$account) {
      return null;
    }
    return $account[0];
  }

  /**
   * @param Account $account
   * @param string $hash
   * @return boolean
   */
  public function checkPassword($account, $password)
  {
    return HashService::checkPassword($password, $account->getPasswordHash());
  }

  /**
   * @param Account $username
   * @return int
   */
  public function getId(Account $account)
  {
    return $this->accountRepository->getId($account->getUsername())[0]['id'];
  }


  /**
   * @param string $token
   * @return Account
   */
  public function getAccountFromToken($token)
  {
    // if token is not set
    if (!$token || $token == '') {
      // return nothing
      return null;
    }
    // get account
    $account = $this->accountRepository->getAccountFromToken($token);
    // check if token exists for account
    if ($account) {
      // return deserialized account
      return Account::deserialize($account[0]);
    } else {
      // return nothing
      return null;
    }
  }

  /**
   * @param Account $account
   * @return AccountService
   */
  public function update(Account $account) {
    $account->load();
    $this->accountRepository->update($account->getId(), $account->getUsername(), $account->getPasswordHash(), $account->getToken(), $account->getTimeOfToken(), $account->getTimeout());
    return $this;
  }

}
