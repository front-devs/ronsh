<?php


/**
 * class Account
 */
class Account
{

  const ACCOUNT_TOKEN_EXPIRE = 60 * 60 * 24;

  /**
   * @var string
   */
  private $username;
  /**
   * @var string
   */
  private $passwordHash;
  /**
   * @var int
   */
  private $id = null;
  /**
   * @var array
   */
  private $rights = null;
  /**
   * @var AccountService
   */
  private $accountService;
  /**
   * @var array
   */
  private $token;
  /**
   * timeOfToken: expireTime
   * @var int
   */
  private $timeOfToken;
  /**
   * @var int
   */
  private $timeout;
  /**
   * @var boolean
   */
  private $loaded = false;
  /**
   * @var boolean
   */
  private $exists;

  /**
   * constructor
   * @param string $username
   * @param string $passwordHash
   */
  public function __construct($username)
  {
    // initialize services
    $this->accountService = new AccountService();
    // setup variables
    $this->username = $username;
  }


  /**
   * @param string $username
   * @return Account
   */
   public function setUsername($username)
   {
     $this->username = $username;
     return $this;
   }

  /**
   * @param string $password
   * @return Account
   */
  public function setPassword($password)
  {
    $this->passwordHash = $password;
    return $this;
  }

  /**
   * @return Account
   */
  public function hashPassword()
  {
    $this->passwordHash = HashService::hashPassword($this->passwordHash);
    return $this;
  }

  /**
   * @param int $id
   * @return Account
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @param string $token
   * @return Account
   */
  public function setToken($token)
  {
    $this->token = $token;
    return $this;
  }

  /**
   * @param int $time
   * @return Account
   */
  public function setTimeOfToken($time)
  {
    $this->timeOfToken = $time;
    return $this;
  }

  /**
   * @param int $timeout
   * @return Account
   */
  public function setTimeout($timeout)
  {
    $this->timeout = $timeout;
    return $this;
  }


  /**
   * @return Account
   */
  public function loadRights()
  {
    $this->rights = new Rights([], $this);
    $this->rights->load($this);
    return $this;
  }

  /**
   * @return Account
   */
  public function load()
  {
    if (!$this->loaded) {
      // load all info
      // get raw info
      $info = $this->accountService->getAccountRaw($this->getUsername());
      // check if account exists
      if (null === $info) {
        $this->exists = false;
        return $this;
      }
      // because of the check we know the account exists
      $this->exists = true;
      // setup all things
      $this
        ->setPassword($info['passwordHash'])
        ->setId($info['id'])
        ->setTimeout(strtotime($info['timeout']))
        ->setToken($info['token'])
        ->setTimeOfToken(strtotime($info['timeOfToken']))
      ;
      // note that it is allready loaded
      $this->loaded = true;
      // load rights
      $this->loadRights();
    }
    return $this;
  }


  /**
   * @return string
   */
  public function getUsername()
  {
    return $this->username;
  }

  /**
   * @return string
   */
  public function getPasswordHash()
  {
    $this->load();
    return $this->passwordHash;
  }

  /**
   * @return int
   */
  public function getId()
  {
    $this->load();
    return $this->id;
  }

  /**
   * @return Rights
   */
  public function getRights()
  {
    if (null === $this->rights) {
      $this->loadRights();
    }
    return $this->rights;
  }

  /**
   * @return string
   */
  public function getToken()
  {
    // load data if not loaded
    $this->load();
    // get token and the time
    $token = $this->token;
    $time = $this->getTimeOfToken();
    // check if need to update token
    if ($time + 60*60*24 < time() || !$token) {
      // generate a new token
      $token = HashService::generateToken($this->getUsername(), $this->getPasswordHash());
      // set token and the expire time
      $this->setToken($token);
      $this->setTimeOfToken(time());
    }
    return $token;
  }

  /**
   * @return string
   */
  public function getTimeOfToken()
  {
    $this->load();
    return $this->timeOfToken;
  }

  /**
   * @return int
   */
  public function getTimeout()
  {
    $this->load();
    return $this->timeout;
  }

  /**
   * @return Account
   */
  public function updateDatabase()
  {
    if ($this->exists()) {
      $this->accountService->update($this);
    }
    return $this;
  }

  public function exists() {
    $this->load();
    return $this->exists;
  }


  /**string $username
   * @param string $hash
   * @return array
   */
  public function toArray()
  {
    return [
      'username' => $this->getUsername(),
      'passwordHash' => $this->getPasswordHash(),
      'rights' => $this->getRights()->serialize(),
      'token' => $this->getToken(),
      'timeOfToken' => $this->getTimeOfToken(),
      'timeout' => $this->getTimeout()
    ];
  }

  /**
   * @return array
   */
  public function serialize()
  {
    // get all infos
    $array = $this->toArray();
    // remove things nobody is allowed to see
    unset($array['passwordHash']);
    // return it  
    return $array;
  }

  /**
   * todo: maybe uncomplete
   * @param array $array
   * @return Account
   */
  public static function deserialize($array) {
    // validate array
    if ($array && array_key_exists('username', $array)) {
      // get username and account
      $username = $array['username'];
      $account = new Account($username);
      // return account
      return $account;
    } else {
      throw new AccountException(AccountException::PARSE_ERROR);
    }
  }


  /* destructor */
  public function __destruct()
  {
    $this->updateDatabase();
  }

}
