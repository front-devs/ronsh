<?php

/**
 * class JsonErrorResponse
 */
class JsonErrorResponse extends JsonResponse
{

  /**
   * @param string $message
   * @param int $statusCode = 500
   */
  public function __construct($message = "", $statusCode = 500)
  {
    // send data to JsonResponse
    parent::__construct(null, $statusCode, $message, false);
  }

}
