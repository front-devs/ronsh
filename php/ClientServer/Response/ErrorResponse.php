<?php

/**
 * class ErrorResponse
 */
class ErrorResponse extends Response
{

  /**
   * @param string $message
   * @param int $statusCode = 500
   */
  public function __construct($message, $statusCode = 500)
  {
    // set response code
    http_response_code($statusCode);
    // set template and template variables
    $template = 'error';
    $template_variables = [
      'statusCode' => $statusCode,
      'message' => $message
    ];
    // send
    parent::__construct($template, $template_variables, $statusCode);
  }

}
