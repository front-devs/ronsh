<?php

/**
 * class JsonResponse
 */
class JsonResponse extends Response
{

  /**
   * @param array $json
   * @param int $statusCode = 200
   * @param string $statusText = ""
   * @param boolean $success = null
   */
  public function __construct($json = null, $statusCode = 200, $statusText = "", $success = null)
  {
    // auto detect success
    if (null === $success) {
      $success = ($statusCode >= 200 && $statusCode < 300);
    }
    // setup response
    $response = [
      'success' => $success,
      'content' => $json,
      'additional' => $statusText
    ];
    // send all things back
    $template = 'json';
    $template_variables = ['response' => json_encode($response)];
    header('Content-Type: application/json;charset=utf-8;');
    parent::__construct($template, $template_variables, $statusCode);
  }

}
