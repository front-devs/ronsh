<?php

/**
 * class Response
 */
class Response
{

   /**
    * @var string $template
    */
   protected $template;
   /**
    * @var string $template_variables
    */
   protected $template_variables;
   /**
    * @var string $statusCode
    */
   protected $statusCode;

  /**
   * @param string $template = null
   * @param array $template_variables = []
   * @param int $statusCode = 200
   */
  public function __construct($template = null, $template_variables = [], $statusCode = 200)
  {
    // setup all things
    $this->setTemplate($template);
    $this->setTemplateVariables($template_variables);
    $this->setHttpStatusCode($statusCode);
  }

  /**
   * super cool function to send the response in a cool way back!
   * @return Response
   */
  public function fire()
  {
    /* init */
    $template = $this->getTemplate();
    $template_variables = $this->getTemplateVariables();
    $statusCode = $this->getHttpStatusCode();

    /* check if template is set */
    if (null === $template) {
      throw new ResponseException(ResponseException::NOT_DEFINED_TEMPLATE);
    }

    /* fire */
    $twig_loader = new Twig_Loader_Filesystem("include");
    $twig_env = new Twig_Environment($twig_loader, [
      'cache' => false
    ]);
    $twig_template = $twig_env->load($this->getTemplate());
    http_response_code($this->getHttpStatusCode());
    echo $twig_template->render($this->getTemplateVariables());
    return $this;
  }


  /**
   * @return string
   */
  public function getTemplate()
  {
    return $this->template;
  }

  /**
   * @return string
   */
  public function getTemplateVariables()
  {
    return $this->template_variables;
  }

  /**
   * @return string
   */
  public function getHttpStatusCode()
  {
    return $this->statusCode;
  }


  /**
   * @param string $template
   * @return Response
   */
  public function setTemplate($template)
  {
    // check args
    if (null !== $template) {
      $template = '/html/templates/layouts/'.$template.'.html';
    }
    // set the template
    $this->template = $template;
    // return me
    return $this;
  }

  /**
   * @param array $template_variables
   * @return Response
   */
  public function setTemplateVariables($template_variables)
  {
    $this->template_variables = $template_variables;
    return $this;
  }

  /**
   * @param int $statusCode
   * @return Response
   */
  public function setHttpStatusCode($statusCode)
  {
    $this->statusCode = $statusCode;
    return $this;
  }

}
