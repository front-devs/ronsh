<?php

/**
 * class ApiController
 */
class ApiController
{
  /**
   * @var ProductService
   */
  private $productService;
  /**
   * @var AccountService
   */
  private $accountService;
  /**
   * @var MarketService
   */
  private $marketService;
  /**
   * @var BranchService
   */
   private $branchService;

  /**
   * constructor
   */
  public function __construct()
  {
    // initialize services
    $this->productService = new ProductService();
    $this->accountService = new AccountService();
    $this->marketService = new MarketService();
    $this->branchService = new BranchService();
    // setup error api
    WebException::setExceptionResponseApi("JsonErrorResponse");
  }


  /**
   * @param Request $request
   * @return Response
   */
   public function test(Request $request)
   {
     $data = $request->getJsonData();
     return new JsonResponse($data);
   }

  /**
   * @param Request $request
   * @return Response
   */
  public function search(Request $request)
  {
    // get data
    $data = $request->getQueryData();
    // check data
    if (!array_key_exists('query', $data)
    || ($query = $data['query']) == '') {
      throw new WebException("please pass a query with a string length greater than 0", 400);
    }
    // load requested data
    $json = $this->productService->search($data['query']);
    // send
    return new JsonResponse($json);
  }

   /**
    * returns info about the requesting user
    * @param Request $request
    * @return Response
    */
   public function getAccountInfo(Request $request)
   {
     // get the user
     $user = $request->getRequestingUser();
     LogService::info("user: " . json_encode($user->serialize()));
     // check if user exists or not spezified
     if (null !== $user) {
       $response = $user->serialize();
     } else {
      throw new WebException("user doesn't exist or wasn't passed", 400);
     }
     // send user information
     return new JsonResponse(['userInfo' => $response]);
   }

   /**
    * adds an account
    * @param Request $request
    * @return Response
    */
   public function addAccount(Request $request)
   {
     // get data of request
     $data = $request->getJsonData();
     $user = $request->getRequestingUser();
     // if user spezified or exists
     if (null !== $user) {
      // get rights of user
      $rights = $user->getRights();
      // check rights for creating account
      if ($rights->getState(AccountRights::RIGHT_CREATE_ACCOUNT)) {
        // validate
        if ( array_key_exists('username', $data)
        &&   array_key_exists('password', $data)
        &&  ($username = $data['username']) != ''
        &&  ($password = $data['password']) != '') {
          // add account
          $account = new Account($username);
          $account->setPassword($password)->hashPassword();
          $this->accountService->addAccount($account);
          // send the account back
          return new JsonResponse($account->serialize());
        } else {
          throw new WebException("username or password field not spezified", 400);
        }
      }
      throw new WebException("you have no rights to add an account", 403);
     }
     throw new WebException("user doesn't exist or wasn't passed", 400);
   }

   /**
    * returns the token of a account
    * @param Request $request
    * @return Response
    */
   public function getAccountToken(Request $request)
   {
     // get data
     $data = $request->getQueryData();
     //validate
     if ( array_key_exists('username', $data)
     &&   array_key_exists('password', $data)
     &&  ($username = $data['username']) != ''
     &&  ($password = $data['password']) != '') {
      // create an account with the username
      $account = new Account($username);
      // check if the account exists and password is right
      if ($account->exists() && $this->accountService->checkPassword($account, $password)) {
        // response
        return new JsonResponse([
          'token' => $account->getToken()
        ]);
      } else {
        throw new WebException("login failed", 403);
      }
     }
     throw new WebException("you need to set username and password", 400);
   }

   /**
    * adds a right to itself
    * todo: this need to be available to do that with other accounts
    * @param Request $request
    * @return Response
    */
    public function addRight(Request $request)
    {
      // get request data
      $data = $request->getJsonData();
      $user = $request->getRequestingUser();
      // check user availability
      if (null !== $user) {
        // get rights of the user
        $userRights = $user->getRights();
        // check rights to change another right
        if ($userRights->getState(AccountRights::RIGHT_CHANGE_ACCOUNT_RIGHT)) {
          // loops through all needed rights and add them
          $rights = $data['rights'];
          foreach ($rights as $r) {
            $type = $r['type'];
            $value = $r['value'];
            $right = new Right($type, $value);
            $userRights->add($right);
          }
          return new JsonResponse(["rights" => $userRights->serialize()]);
        }
        throw new WebException("you have no rights to add a right", 403);
      }
      throw new WebException("user doesn't exist or wasn't passed", 400);
    }

    /**
    * @param Request $request
    * @return Response
    */
    public function getMarketsInfo(Request $request)
    {
      // get request data
      $data = $request->getJsonData();
      $user = $request->getRequestingUser();
      // checking user availability
      if (null !== $user) {
        // get the rights to know all markets the user has access
        $rights = $user->getRights();
        $marketsAccess = $rights->getStates(AccountRights::RIGHT_WRITE_MARKET);
        // get them
        $markets = $this->marketService->getMarkets($marketsAccess);
        // send
        return new JsonResponse(['markets' => $markets]);
      }
      throw new WebException("user doesn't exist or wasn't passed", 400);
    }

    /**
    * @param Request $request
    * @return Response
    */
    public function getBranchesInfo(Request $request)
    {
      // get request data
      $data = $request->getJsonData();
      $user = $request->getRequestingUser();
      // checking user availability
      if (null !== $user) {
        // get rights to know branches user can access
        $rights = $user->getRights();
        $branchesAccess = $rights->getStates(AccountRights::RIGHT_WRITE_BRANCH);
        // get branches
        $branches = $this->branchService->getBranches($branchesAccess);
        // send them
        return new JsonResponse(['branches' => $branches]);
      }
      throw new WebException("user doesn't exist or wasn't passed", 400);
    }

    /**
    * @param Request $request
    * @return Response
    */
    public function addProduct(Request $request)
    {
      // get data
      $data = $request->getQueryData();
      $user = $request->getRequestingUser();
      // checking user availability
      if (null !== $user) {
        // get rights
        $rights = $user->getRights();
        // validate
        if (array_key_exists('market', $data)
        &&  array_key_exists('branch', $data)
        &&  ($market = $data['market']) != ''
        &&  ($market = $data['branch']) != '') {
          // check rights
          if (($rights->getState(AccountRights::RIGHT_WRITE_MARKET) == 0 || $rights->checkState(AccountRights::RIGHT_WRITE_MARKET, $market))
          &&  ($rights->getState(AccountRights::RIGHT_WRITE_BRANCH) == 0 || $rights->checkState(AccountRights::RIGHT_WRITE_BRANCH, $branch))) {
            // init
            $config = ConfigService::getConfig();
            $image_index = $config['image']['index'] + 1;
            $image = $request->getFile('image');
            $product = json_decode($data['product'], true);
            // update
            if (move_uploaded_file($image['tmp_name'], ConfigService::getRootDir().'/database/productImages/'.dechex($image_index).'.jpg')) {
              $this->productService->addProduct($market, $branch, $product, $image_index);
            } else {
              //todo: error (failed to get image)
              throw new WebException("failed to get image", 500);
            }
            $config['image']['index'] = $image_index;
            ConfigService::setConfig($config);
            // send
            // todo: send the product-id back!
            return new JsonResponse();
          }
          throw new WebException("you have no right to add a product", 500);
        } else {
          throw new WebException("you didn't pass market and branch", 400);
        }
      }
      throw new WebException("user doesn't exist or wasn't passed", 400);
    }

}
