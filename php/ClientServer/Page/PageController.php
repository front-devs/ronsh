<?php

/**
 * class PageController
 */
class PageController
{

  /**
   * @var ProductService
   */
  private $productService;

  /**
   * constructor
   */
  public function __construct()
  {
    // initialize services
    $this->productService = new ProductService();
    // setup error api
    WebException::setExceptionResponseApi("ErrorResponse");
  }

  /**
   * redirects to search
   * @param Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    return $this->search($request);
  }

  /**
   * @param Request $request
   * @return Response
   */
  public function search(Request $request)
  {
    // get data
    $data = $request->getQueryData();
    // checking data
    if (array_key_exists('q', $data) && ($query = $data['q']) != '') {
      $entries = $this->productService->search($query);
    } else {
      $query = null;
      $entries = null;
    }
    // sending data back
    return new Response('default', [
      'title' => 'Suche',
      'page' => 'index',
      'search_query' => $query,
      'entries' => $entries
    ]);
  }


  /**
   * @param Request $request
   * @return Response
   */
  public function productView(Request $request) {
    // get data
    $data = $request->getQueryData();
    // check data
    if (!array_key_exists('product', $data) || !($result = $this->productService->getProduct($data['product']))) {
      throw new WebException('product not found', 404);
    }
    // send back
    return new Response("default", [
      'page' => 'product_view',
      'title' => 'Produktansicht',
      'product' => $result
    ]);
  }

  /**
   * @param Request $request
   * @return Response
   */
  public function account(Request $request) {
    // only send page back
    return new Response("default", [
      'page' => 'account',
      'title' => 'Produktansicht'
    ]);
  }

}
