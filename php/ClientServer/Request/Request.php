<?php

/**
 * Request
 */
class Request {

  /**
   * @var array
   */
  private $data;
  /**
   * @var Route
   */
  private $route;
  /**
   * @var string
   */
  private $method;
  /**
   * @var Account
   */
  private $requestingUser;
  /**
   * @var AccountService
   */
  private $accountService;


  /**
   * @param Route $route
   * @param string $method
   */
  public function __construct($route, $method, $token)
  {
    // initialize services
    $this->accountService = new AccountService();
    // setup variables
    $this->route = $route;
    $this->method = $method;
    $this->data = $this->methodGetData($method);
    $this->requestingUser = $this->accountService->getAccountFromToken($token);
  }

  /**
   * @param string $method
   * @return array
   */
  private function methodGetData($method)
  {
    // init $data
    $data = ['query'=>null, 'json'=>null];
    // get right data for request
    switch (strtoupper($method)) {
      case 'POST':
        $data['query'] = $_POST;
        break;
      case 'GET':
      case 'PUT':
      case 'DELETE':
        $data['query'] = $_GET;
        break;
      default:
        throw new RequestException(RequestException::UNKNOWN_METHOD);
    }
    // put json to it
    $data['json'] = json_decode(file_get_contents('php://input'), true);
    // return the array
    return $data;
  }

  /**
   * todo: maybe rename getRoute of Route to getUrl
   * @return string
   */
  public function getUrl() {
    return $this->route->getRoute();
  }

  /**
   * @return string
   */
  public function getMethod() {
    return $this->method;
  }

  /**
   * get the data what methodGetData returned
   * @return array
   */
  public function getQueryData() {
    return $this->data['query'];
  }

  /**
   * get the data what methodGetData returned
   * @return array
   */
  public function getJsonData() {
    return $this->data['json'];
  }

  /**
   * @return Route
   */
  public function getRoute() {
    return $this->route;
  }

  /**
   * @return Account
   */
  public function getRequestingUser() {
    return $this->requestingUser;
  }

  /**
   * @param string $filename
   * @return array|null
   */
   public function getFile($filename) {
    // check if file was uploaded
    if (array_key_exists($filename, $_FILES)) {
      // return the file
      return $_FILES[$filename];
    }
    // return nothing
    return null;
  }

}
