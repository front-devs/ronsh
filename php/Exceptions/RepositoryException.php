<?php

/**
 * class RepositoryException
 */
class RepositoryException extends WebException
{
  /* Defining Exceptions */
  const PDO_ERROR = 0;

  /**
   * @param int $type
   * @param int $httpStatusCode
   */
  public function __construct($type = Exception::UNKNOWN, $additional = "")
  {
    switch ($type) {
      case self::PDO_ERROR:
        parent::__construct("pdo error", 500, $type, $additional);
        break;
      default:
        parent::__construct();
    }
  }

}
