<?php

/**
 * class RequestException
 */
class RequestException extends WebException
{
  /* Defining Exceptions */
  const UNKNOWN_METHOD = 0;

  /**
   * @param int $type
   * @param int $httpStatusCode
   */
  public function __construct($type = Exception::UNKNOWN, $httpStatusCode = 500, $additional = "")
  {
    switch ($type) {
      case self::UNKNOWN_METHOD:
        parent::__construct("using not known method", 500, $type, $additional);
        break;
      default:
        parent::__construct();
    }
  }

}
