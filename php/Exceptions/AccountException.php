<?php

/**
 * class AccountException
 */
class AccountException extends WebException
{
  /* Defining Exceptions */
  const PARSE_ERROR = 0;

  /**
   * @param int $type
   * @param int $httpStatusCode
   */
  public function __construct($type = Exception::UNKNOWN, $additional = "")
  {
    switch ($type) {
      case self::PARSE_ERROR:
        parent::__construct("couldn't parse input", 500, $type, $additional);
        break;
      default:
        parent::__construct();
    }
  }

}
