<?php

/**
 * class ResponseException
 */
class ResponseException extends WebException
{
  /* Defining Exceptions */
  const NOT_DEFINED_TEMPLATE = 0;

  /**
   * @param int $type
   * @param int $httpStatusCode
   */
  public function __construct($type = Exception::UNKNOWN, $httpStatusCode = 500, $additional = "")
  {
    switch ($type) {
      case self::NOT_DEFINED_TEMPLATE:
        parent::__construct("firing response without setting template", 500, $type, $additional);
        break;
      default:
        parent::__construct();
    }
  }

}
