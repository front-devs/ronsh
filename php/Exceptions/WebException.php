<?php

/* set exception kinds */
error_reporting(E_ALL);

/* set exception handler */
set_exception_handler(function($exception) {
  // set default values
  $response_message = "Internal Server Error";
  $response_statusCode = 500;
  // check if exception compatible
  if (method_exists($exception, 'getHttpStatusCode')) {
    // overwrite default values if status-code not 500
    $status = $exception->getHttpStatusCode();
    if ($status != 500) {
      $response_message = $exception->getMessage();
      $response_statusCode = $status;
      LogService::unimportantError($exception."");
    } else {
      LogService::error($exception."");
    }
  }
  // send the exception as response
  $errorClass = WebException::$exceptionResponseApi;
  $response = new $errorClass($response_message, $response_statusCode);
  $response->fire();
});

/**
 * class WebException
 */
class WebException extends Exception
{

  // default vars
  protected $message;
  protected $code;
  protected $file;
  protected $line;
  private $additional = "";
  private $httpStatusCode;
  private $exception;
  private $trace;
  private $previous = null;
  private $string;

  // api for response as exeption
  public static $exceptionResponseApi = "ErrorResponse";

  const UNKNOWN = -1;

  public function __construct($message = "Unknown", $httpStatusCode = 500, $code = -1, $additional = "")
  {
    $this->httpStatusCode = $httpStatusCode;
    $this->code = $code;
    $this->message = $message;
    if (strlen($additional)) {
      $this->additional = ':\n'.$additional;
    }
  }

  /**
   * @return int
   */
  final public function getHttpStatusCode() {
    return $this->httpStatusCode;
  }

  // setters
  /**
   * @param string $api as class
   */
  public static function setExceptionResponseApi($api) {
    self::$exceptionResponseApi = $api;
  }

  // Overrideables
  /**
   * @return string
   */
  public function __toString() {
    return get_class($this).": " . $this->getMessage() . $this->additional . "\nReturning with status code " . $this->getHttpStatusCode() . "\nStackTrace:" . $this->getTraceAsString();
  }

}
