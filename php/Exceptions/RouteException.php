<?php

/**
 * class RouteException
 */
class RouteException extends WebException
{
  /* Defining Exceptions */
  const NOT_DEFINED_CONTROLLER = 0;

  /**
   * @param int $type
   * @param int $httpStatusCode
   */
  public function __construct($type = Exception::UNKNOWN, $httpStatusCode = 500, $additional = "")
  {
    switch ($type) {
      case self::NOT_DEFINED_CONTROLLER:
        parent::__construct("trying to create route without defining controller", 500, $type, $additional);
        break;
      default:
        parent::__construct();
    }
  }

}
