<?php

/**
 * Import all files needed
*/

/* Controller */
require(__DIR__."/ClientServer/API/ApiController.php");
require(__DIR__."/ClientServer/Page/PageController.php");

/* ClientServer */
// Request
require(__DIR__."/ClientServer/Request/Request.php");
// Response
require(__DIR__."/ClientServer/Response/Response.php");
require(__DIR__."/ClientServer/Response/JsonResponse.php");
require(__DIR__."/ClientServer/Response/JsonErrorResponse.php");
require(__DIR__."/ClientServer/Response/ErrorResponse.php");

/* Twig */
require(__DIR__.'/Twig1/lib/Twig/Autoloader.php');

/* Workers */
// Cookie
require(__DIR__."/Worker/Cookie/CookieService.php");
// Location
require(__DIR__."/Worker/Location/LocationService.php");
// Repository
require(__DIR__."/Worker/Repository/ProductRepository.php");
require(__DIR__."/Worker/Repository/BranchRepository.php");
require(__DIR__."/Worker/Repository/MarketRepository.php");
require(__DIR__."/Worker/Repository/AccountRepository.php");
require(__DIR__."/Worker/Repository/RightRepository.php");
require(__DIR__."/Worker/Repository/RepositoryService.php");
// Route
require(__DIR__."/Worker/Routes/Route.php");
require(__DIR__."/Worker/Routes/RouteLoader.php");
require(__DIR__."/Worker/Routes/UrlBeautifuller.php");
// Config
require(__DIR__."/Worker/Config/ConfigService.php");
// Product
require(__DIR__."/Worker/Product/ProductService.php");
// Branch
require(__DIR__."/Worker/Branch/BranchService.php");
// Market
require(__DIR__."/Worker/Market/MarketService.php");
// Money
require(__DIR__."/Worker/Money/CurrencyService.php");
// Account
require(__DIR__."/Worker/Account/AccountService.php");
require(__DIR__."/Worker/Account/Account.php");
// Rights
require(__DIR__."/Worker/Rights/Right.php");
require(__DIR__."/Worker/Rights/Rights.php");
require(__DIR__."/Worker/Rights/AccountRights.php");
require(__DIR__."/Worker/Rights/RightService.php");
// Hash
require(__DIR__."/Worker/Hash/HashService.php");
// Log
require(__DIR__."/Worker/Log/LogService.php");

/* Exceptions */
require(__DIR__."/Exceptions/WebException.php");
require(__DIR__."/Exceptions/RouteException.php");
require(__DIR__."/Exceptions/RequestException.php");
require(__DIR__."/Exceptions/ResponseException.php");
require(__DIR__."/Exceptions/RepositoryException.php");
require(__DIR__."/Exceptions/AccountException.php");


/**
 * INIT
 */

/* Twig */
Twig_Autoloader::register();



/**
 * Load routes and use them
 */

$url = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];
$headers = getallheaders();
if (array_key_exists('X-RONSH-TOKEN', $headers)) {
  $token = $headers['X-RONSH-TOKEN'];
} else if (array_key_exists('X-Ronsh-Token', $headers)) {
  $token = $headers['X-Ronsh-Token'];
} else {
  $token = "";
}



$routeLoader = new RouteLoader(__DIR__."/res/routes.json");
$route = $routeLoader->getRoute($url, $method);

/* throw exception if route doesn't exists */
if (null === $route) {
  throw new WebException("Page not found", 404);
}


/**
 * call the controller and handle Response
 */

/* init */
$controller = $route->getController();
$function = $route->getFunction();

$controller = new $controller;
$request = new Request($route, $method, $token);
$response = $controller->$function($request);

/* aaaand finally send the nice things back! */
$response->fire();
