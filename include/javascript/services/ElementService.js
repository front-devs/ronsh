


ServiceLocator.addService('ElementService', [
  function() {
    // create export variable
    var exports = {};

    // function to get the elements that are in the scopes (scope)
    function elementGetter(element, scope) {
      // declare local variables
      var result, len, scopes, gotElements, len2, i, j;
      // create result list for elements
      result = [];
      // check if scope is an array
      if (scope instanceof NodeList) {
        // set scopes to scope
        scopes = scope;
      } else {
        // create an array of scope and set it to scopes (to loop through the scopes)
        scopes = [scope];
      }
      // check if there are any scopes
      if (scopes.length) {
        // get length for loop
        len = scopes.length
        // loop through all scopes and add the elements of the scopes to the result list
        for (i = 0; i < len; ++i) {
          scope = scopes[i];
          // get all elements at the scope scopes[i] with querySelectorAll
          gotElements = scope.querySelectorAll(element);
          // if the element is already in the scope add it
          // get length for loop
          len2 = gotElements.length;
          // loop to add all elements to the result
          for (j = 0; j < len2; ++j) {
            // add element to the result
            result.push(gotElements[j]);
          }
        }
        // if there is only one element
        if (result.length === 1) {
          // set the result to the one element without having an array
          result = result[0];
        }
        // return the result
        return result;
      }
      // return nothing if no scope was passed
      return null;
    }


    // function to get the elements, that are defined in a complicated array structure
    function getElements(element, scope) {
      // declare local variables
      var result, name, e;
      // create result object
      result = {};
      // check if the element is an array
      if (Array.isArray(element)) {
        // the element is actually at the index 0 and continuing elements at index 1 as object
        // get the element (maybe result will be an array. Need to improve this part some time if needed)
        result = getElements(element[0], scope); // uses querySelector
        // set the scope to the current got element
        scope = result;
        // add an attribute to the element: elements which gets the content of the parsed given object
        result.elements = getElements(element[1], scope);
      } else {
        // checks if is an object or only one string (string to get the element)
        if (typeof(element) === "object") {
          // loop through all elements that are wanted
          for (name in element) {
            // get the elements name
            e = element[name];
            // add the element to the result
            result[name] = getElements(e, scope);
          }
        } else {
          // get the element from string
          result = elementGetter(element, scope);
          // if the element doesn't exist
          if (null === result) {
            // throw an error
            throw "element " + element + " does not exists";
          }
        }
      }
      return result;
    }

    // make getElements global (and easier)
    exports.getElements = function(elements) {
      return getElements(elements, document);
    }

    return exports;
  }
])
