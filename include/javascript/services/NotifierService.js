

ServiceLocator.addService('NotifierService', [
    function() {
        // declare local variables
        var exports, notificationContainer;
        // setup variables
        exports = {};
        notificationContainer = undefined;

        // displays a green notification
        exports.success = function(message, timeout) {
            notify(message, 'success', timeout);
        }

        // displays a blue notification
        exports.info = function(message, timeout) {
            notify(message, 'info', timeout);
        }

        // displays an orange notification
        exports.warning = function(message, timeout) {
            notify(message, 'warning', timeout);
        }

        // displays a red notification
        exports.error = function(message, timeout) {
            notify(message, 'error', timeout);
        }

        // displays a progress notification
        exports.progress = function(message, timeout) {
            // declare local variables
            var element;
            // add a notification with html in it
            element = notify(message, "progressbar", -1, "<p>" + message + "</p><div class='progressbar'></div>");
            // then return an object with many information in it
            return {
                element: element.getElementsByClassName('progressbar')[0],
                timeout: timeout || 1000,
                // set a progress-value in percent (between 0 and 1)
                setValue: function(value) {
                    // refreshes the displaying progress 
                    this.element.style.width = (value * 100) + "%";
                    // if finished
                    if (value >= 1) {
                        // creating a delay to remove the notification
                        setTimeout(function() {
                            this.element.remove();
                        }, this.timeout);
                        // call onfinish event
                        if (this.onfinish) {
                            this.onfinish();
                        }
                    }
                    return this;
                },
                // function to use xhr progress
                useXhr: function(xhr) {
                    // declare local variables
                    var notify;
                    notify = this;
                    // event: on progress of the xhr
                    xhr.upload.onprogress = function(event) {
                        // check if the progress can be calculated
                        if (event.lengthComputable) {
                            // refresh progress
                            notify.setValue(event.loaded / event.total);
                        }
                    }
                    // event: on finish uploading, sets progress to 100%
                    xhr.upload.onloadend = function() {
                        notify.setValue(1);
                    }
                },
                onfinish: null
            }
        }

        // function to notify
        function notify(message, kind, timeout, html) {
            // declare local variables
            var element, notifierBlock;
            // create the element
            element = document.createElement("DIV");
            // give the element an color attribute (kind-...) and mark it as an entry
            element.className = "entry kind-" + kind;
            // set the html of the notification element
            element.innerHTML = html || "<p class='content'>" + message + "</p>";
            // get the container
            notificationContainer = getNotificationContainer();
            // if there are already some notifications active insert this one at the first pos. Else append it (can't insert before an undefined element)
            if (notificationContainer.children.length) {
                notificationContainer.insertBefore(element, notificationContainer.children[0]);
            } else {
                notificationContainer.appendChild(element);
            }
            // set a timer to remove this entry later
            if (timeout != -1) {
                setTimeout(function() {
                    element.remove();
                }, timeout || 5000);
            }
            // return this element
            return element;
        }

        // get container of all notifications
        function getNotificationContainer() {
            notificationContainer = notificationContainer || document.getElementById("notificationContainer");
            return notificationContainer;
        }

        return exports;
    }
])