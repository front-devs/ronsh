

ServiceLocator.addService('InputService', [
  function() {
    // declare local variables
    var exports, listener;
    // setup exports and listener
    exports = {};
    listener = [];

    // event that is better (dectects a change directly, you can configure a delay)
    exports.onChange = function(inputElement, delay, onchange) {
      // declare local variables
      var lst;
      // setup listener object
      lst = {
        'element': inputElement,
        'delay': delay,
        'onchange': onchange,
        'value': inputElement.value,
        'timer': null
      };
      // give the input an attribute with the listener. Needed for the event
      inputElement.InputServiceListener = lst;
      // event: stopping holding a key
      inputElement.onkeyup = function(e) {
        // declare local variables
        var listener;
        // set listener to the listener that is set before for this element
        listener = this.InputServiceListener;
        // check if the value has changed
        if (this.value !== listener.value) {
          // set the listener value to the correct value
          listener.value = this.value;
          // stop timer if there was one
          if (listener.timer) {
            clearTimeout(listener.timer);
          }
          // start a new timer (for delay)
          listener.timer = setTimeout(listener.onchange, listener.delay);
        }
      };
      // add this listener to all other listeners
      listener.push(lst);
      // return this listener
      return lst;
    }

    // return all public things
    return exports;
  }
])
