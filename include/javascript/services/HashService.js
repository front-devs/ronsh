

ServiceLocator.addService('HashService', [
  function() {
    // create export object
    var exports = {};
    
    // function to hash a string
    exports.hash = function(str) {
      // declare local variables
      var hash, i, char;
      // set default value
      hash = 0;
      // if string has no length return 0 (hash was initialized with 0)
      if (!str.length) {
        return hash;
      }
      // loop through all characters of the string and calculate the hash with every of this
      for (i = 0; i < str.length; ++i) {
        // get current char
        char = str.charCodeAt(i);
        // calculate hash
        hash = ((hash << 5) - hash) + char;
        // converts hash to 32 Bit Integer
        hash |= 0;
      }
      // return the hash
      return hash;
    }

    // function converts a number to a hex (using built-in functions)
    exports.toHex = function(num) {
      return (+num).toString(16);
    }

    // return public functions
    return exports;
  }
])
