

ServiceLocator.addService('ProductService', [
    'AccountService',
    'NotifierService',
    function(AccountService, NotifierService) {
      var exports = [];
      
      exports.addProduct = function (market, branch, name, description, price, image) {
        var xhr;
        xhr = AccountService.postFile("products", "image", image, {
            'market': market,
            'branch': branch,
            'product': {
              'name': name,
              'description': description,
              'price': price
            }
        })
        var progressbar = NotifierService.progress("Hochlade-Fortschritt");
        progressbar.useXhr(xhr);
        xhr.doIt();
        console.log(xhr);
        return xhr;
      }
      
  
      return exports;
    }
  ])
  