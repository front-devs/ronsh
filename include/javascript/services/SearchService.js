

ServiceLocator.addService('SearchService', [
  'RequestService',
  'TemplateService',
  function(RequestService, TemplateService) {
    var exports = {};
    var searchEntryTemplate = undefined;

    function getSearchEntryTemplate() {
      return RequestService.getFile('html/templates/pages/search_entry.html')
        .then(function(response) {
          searchEntryTemplate = response.data;
          return searchEntryTemplate;
        })
    }

    exports.getProducts = function(searchQuery) {
      return RequestService.get('search?query='+encodeURIComponent(searchQuery)).then(function(response) {
        return response.data;
      });
    };

    exports.getHTML = function(searchResult) {
      if (searchResult.length) {
        if (!searchEntryTemplate) {
          return getSearchEntryTemplate()
            .then(function() {return exports.getHTML(searchResult)});
        } else {
          return new Promise(function(resolve) {
            var html, template, preparedTemplate, len, i;
            html = '';
            template = searchEntryTemplate;
            preparedTemplate = TemplateService.prepare(template);
            len = searchResult.length;
            for (i = 0; i < len; ++i) {
              html += TemplateService.renderPrepared(preparedTemplate, {'entry': searchResult[i]});
            }
            resolve(html);
          });
        }
      } else {
        return new Promise(function(resolve, reject) {
          resolve("<p>Keine Suchergebnisse gefunden...</p>");
        })
      }
    };

    exports.setUrl = function(query) {
      if (query.length) {
        history.replaceState({}, '', '?q='+encodeURIComponent(query));
      } else {
        history.replaceState({}, '', window.location.pathname);
      }

    };


    return exports;
  }
]);
