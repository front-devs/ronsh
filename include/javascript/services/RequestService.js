
ServiceLocator.addService('RequestService', [
  function() {
    var exports = {};
    var ip = "/";
    var ipExtend = 'api/';

    function request(url, method, json, full, headers) {
      return new Promise(function(resolve, reject) {
        if (!full) {
          url = ip+ipExtend+url;
        }
        //console.log("running request: " + url);
        var xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        if (headers) {
          for (i in headers) {
            xhr.setRequestHeader(i, headers[i]);
          }
        }
        xhr.onerror = function(error) {
          reject(error);
        }
        xhr.onload = function() {
          // declare vars as local
          var parsed, response, status, additional, data, success;
          // set variable to response
          parsed = xhr.responseText;
          // try to parse to json
          try {
            // parse to json
            parsed = JSON.parse(parsed);
            // setup variables from json
            additional = parsed.additional;
            data = parsed.content;
            success = parsed.success;
          } catch(e) {
            // if couldn't parse the response is no json
            // setup despite of that variables
            success = (xhr.status >= 200 && xhr.status < 300);
            additional = xhr.statusText;
            data = parsed;
          }
          // setup other variables
          status = xhr.status;
          // catch status 500
          if (status === 500) {
            console.warn("oh, the server seems to be ill...");
          }
          // create return object called response
          response = {
            'status': xhr.status,
            'success': success,
            'additional': additional,
            'url': url,
            'method': method,
            'data': data
          };
          // give it to the promise
          if (status >= 200 && status < 300) {
            resolve(response);
          } else {
            xhr.onerror(response);
          }
        }

        if (json) {
          xhr.send(JSON.stringify(json));
        } else {
          xhr.send();
        }

      });
    }

    exports.get = function(url, headers) {
      return request(url, "GET", undefined, false, headers);
    }

    exports.getFile = function(url) {
      return request(ip+'include/'+url, "GET", undefined, true);
    }

    exports.postFile = function(url, filename, file, parameters, headers) {
      var xhr, i, data, val;
      url = ip+ipExtend+url;
      xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      if (headers) {
        for (i in headers) {
          xhr.setRequestHeader(i, headers[i]);
        }
      }
      data = new FormData();
      data.append(filename, file);
      if (typeof(parameters) == 'object') {
        for (val in parameters) {
          if (typeof(parameters[val]) == "object" || Array.isArray(parameters[val])) {
            parameters[val] = JSON.stringify(parameters[val])
          }
          data.append(val, parameters[val]);
        }
      }
      xhr.doIt = function() {this.send(data)};
      return xhr;
    }

    exports.request = request;

    return exports;
  }
])
