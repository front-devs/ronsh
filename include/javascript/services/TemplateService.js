

ServiceLocator.addService('TemplateService', [
  function() {
    var exports = {};

    function parseExpression(expression, variables) {
      console.log("test", expression, variables);
      var vars, i, len, str;
      str = '';
      expression = expression.replace(/ /g, '');
      vars = expression.split('.');
      len = vars.length;
      if (len) {
        for (i = 0; i < len && variables; ++i) {
          variables = variables[vars[i]];
        }
        if (variables) {
          str = variables;
        } else {
          str = '';
          console.warn("couldn't find variable '" + expression + "'");
        }
      }
      return str;
    }

    function getExpressionMatches(str) {
      var matches, match, regex;
      matches = [];
      regex = /{{[^}}]*}}/g;
      while(match = regex.exec(str)) {
        matches.push(match);
      }
      return matches;
    }

    exports.render = function(str, variables) {
      console.log(str);
      return exports.renderPrepared(exports.prepare(str), variables);
    }

    exports.renderPrepared = function(prepared, variables) {
      console.log("test2", prepared, variables);
      var matches, len, i, str;
      str = prepared.str;
      matches = prepared.matches;
      len = matches.length;
      for (i = 0; i < len; ++i) {
        str = str.replace(matches[i], parseExpression(matches[i][0].slice(2,-2), variables));
      }
      return str;
    }

    exports.prepare = function(str) {
      var prepared;
      prepared = {'str': str,'matches': getExpressionMatches(str)};
      return prepared;
    }

    return exports;
  }
])
