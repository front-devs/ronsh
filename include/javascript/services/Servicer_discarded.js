
/**
 * class Servicer
 */
class Servicer
{

  /**
   * constructor
   */
  constructor()
  {
    this.services = [];
  }

  /**
   * @param string serviceName
   * @param array service
   */
  addService(serviceName, service)
  {
    this.services.push(service);
  }

  /**
   * @var string serviceName
   */
   getService(serviceName)
   {
     return this.services[serviceName];
   }

}
