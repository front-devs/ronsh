

ServiceLocator.addService('AccountService', [
  'RequestService',
  function(RequestService) {
    // declare local variables
    var exports, information;
    exports = [];
    information = {};

    // function to get the saved token
    function getToken() {
      var token = localStorage.accountToken;
      if (token) {
        return token;
      } else {
        return null;
      }
    }

    // function to save the token
    function setToken(token) {
      localStorage.accountToken = token;
    }

    // function to create a request (RequestService.request) where the token is set (already logged in then)
    function request(url, method, json, full, header) {
      // declare local variables
      var header, token;
      // get the token
      token = getToken();
      // if the token is set
      if (token) {
        // pass the token with the header
        header = {'X-RONSH-TOKEN': token};
      }
      // if no method is set
      if (!method) {
        // set the method to "GET"
        method = "GET";
      }
      // return the request
      return RequestService.request(url, method, json, full, header);
    }

    // function to login with the token. Returns -1 if no token is set
    exports.loginWithToken = function() {
      // declare local variables
      var token;
      // get the token
      token = getToken();
      // if there is no token
      if (!token) {
        // return -1 (no token is set)
        return -1;
      }
      // else return the request where the information of the account will be requested
      return request("accounts/account/info")
        .then(function(response) {
          // get account for this service
          information.account = response.data.userInfo;
          // check if the information is right
          if (information.account) {
            console.log("logged in with token");
            // request markets
            return request("accounts/account/info/market")
              .then(function (response) {
                // set markets
                information.markets = response.data.markets;
                // request branches
                return request("accounts/account/info/branch")
                  .then(function (response) {
                    // set branches
                    information.branches = response.data.branches;
                    // send status 'all correct'
                    return true;
                  });
              });
          } else {
            // couldn't get information. Output that and return false (failed to login)
            console.warning("couldn't login because info is null");
            return false;
          }
          
        })
        .catch(function(response) {
          // there was an error. return false (failed to login)
          return false;
        });
    }

    // function to login with username and password
    exports.login = function(username, password) {
      // perform a get request to get the token
      return RequestService.get("accounts/account/token?username="+encodeURIComponent(username)+"&password="+encodeURIComponent(password))
        .then(function(response) {
          // success! Now set the token to get it later back
          setToken(response.data.token);
          // return the success
          return true;
        })
        .catch(function(response) {
          // something went wrong. Return that
          return false;
        })
    }

    // function to get information about the account you are logged in
    exports.getInformation = function() {
      return information;
    }

    // function to post a file with token
    exports.postFile = function(url, filename, file, parameters) {
      // post the file with the RequestService
      return RequestService.postFile(url, filename, file, parameters, {'X-RONSH-TOKEN': getToken()});
    }

    // make request function public
    exports.request = request;

    // return public things
    return exports;
  }
])
