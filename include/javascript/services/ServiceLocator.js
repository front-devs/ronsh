
var ServiceLocator = {};
ServiceLocator.services = {};
ServiceLocator.addService = function(serviceName, service) {
  var i, len, args;
  args = [];
  len = service.length;
  for (i = 0; i < len-1; ++i) {
    args.push(ServiceLocator.getService(service[i]));
  }
  ServiceLocator.services[serviceName] = service[len-1].apply(null, args);
}
ServiceLocator.getService = function(serviceName) {
  var service;
  service = ServiceLocator.services[serviceName];
  if (!service) {
    throw "Service '" + serviceName + "' does not exists!";
  }
  return service;
}
