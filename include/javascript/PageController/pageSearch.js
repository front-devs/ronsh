

PageController.addController('index', [
  'ElementService',
  'InputService',
  'SearchService',
  function(ElementService, InputService, SearchService) {
    // declare local variables
    var elements;
    // setup all elements
    elements = ElementService.getElements({
      'searchArea': "#searchArea",
      'searchbar': "#searchbar",
      'searchbarSubmitButton': "#searchbarSubmitButton",
      'selectorCategory': "#selectorCategory",
      'searchEntries': "#searchEntries"
    });


    // event: onfocus, for the searchbar. It will set the focus-attribute of the searchArea to true
    elements.searchbar.onfocus = function() {
      // set focus of searchArea (moves area to the top)
      elements.searchArea.setAttribute("focus", "true");
    }

    // event: onblur, removes the focus of searchArea (moves area to the center)
    elements.searchbar.onblur = function() {
      // check if there is still a value in the searchbar
      if (!this.value.length) {
        // if not move the area down (center)
        elements.searchArea.setAttribute("focus", "false");
      }
    }

    // event: onchange, searches for a product when typing something in the searchbar
    InputService.onChange(elements.searchbar, 50, function() {
      // set url in the location bar
      SearchService.setUrl(elements.searchbar.value);
      // checks if something is at the searchbar typed in
      if (elements.searchbar.value.length) {
        // get the products for that searchquery
        SearchService.getProducts(elements.searchbar.value)
          .then(function(products) {
            // now get the html of the products
            SearchService.getHTML(products)
              .then(function(html) {
                // if there is still a need for the products
                if (elements.searchbar.value.length) {
                  // display all got products
                  elements.searchEntries.innerHTML = html;
                }
              });
          });
      } else {
        // if there is nothing typed in remove all entries
        elements.searchEntries.innerHTML = '';
      }
    });


  }
])
