
// init controller as object
var PageController = {};
// create list of sub-controllers
PageController.controller = [];
// function to add a controller
PageController.addController = function(page, controller, use) {
  // add the controller
  PageController.controller[page] = controller;
  // check if it should be used directly
  if (use) {
    // use the added controller
    PageController.useController(page);
  }
};
// function to use a controller
PageController.useController = function(controllerName) {
  // declare local variables
  var i, len, args, controller, scope;
  // get the controller
  controller = PageController.getController(controllerName);
  // create list of arguments
  args = [];
  // get length of controller to loop through it and give all services needed back
  len = controller.length;
  // loop through the services the controller need
  for (i = 0; i < len-1; ++i) {
    // add the current service to the argument list
    args.push(ServiceLocator.getService(controller[i]));
  };
  // run the controller and get the returned value as 'scope' back
  scope = controller[len-1].apply(null, args);
  // add scope to the window-this
  // check if the scope is an object so that there is something to add to the this
  if (scope && typeof(scope) == 'object') {
    // loop through the scope
    for (a in scope) {
      // and add every scope to the window
      window[a] = scope[a];
    }
    // if someone want to remove the current scope he can do it with PageController.removeCurrentScope()
    PageController.removeCurrentScope = function() {
      PageController.removeScope(scope);
    }
  }
};
// function to get the controller
PageController.getController = function(controllerName) {
  // declare local variables
  var controller;
  // get the controller
  controller = PageController.controller[controllerName];
  // if the controller doesn't exist
  if (!controller) {
    // throw an error
    throw "Controller '" + controllerName + "' does not exists!";
  }
  // else return the controller
  return controller;
};
// this function removes all object keys of the argument scope in the window
PageController.removeScope = function(scope) {
  for (a in scope) {
    delete window[a];
  }
};
// function will be filled in useScope
PageController.removeCurrentScope = function() {};