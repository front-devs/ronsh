

PageController.addController('account', [
  'ElementService',
  'AccountService',
  'TemplateService',
  'ProductService',
  'NotifierService',
  function(ElementService, AccountService, TemplateService, ProductService, NotifierService) {
    // declare variables as local
    var elements, focussed_window, scope, isLoggedIn;

    // init variables
    focussed_window = null;
    scope = {};
    isLoggedIn = false;

    // put all elements that are needed in the elements variable
    elements = ElementService.getElements({
      "loadscreen": "#loadscreen",

      "windows": [".windows", {
        "accountDetails": [".window-account_details", {
          "header": ".window-header",
          "body": [".window-content", {
            "username": ".information-username"
          }]
        }],
        "login": [".window-login", {
          "header": ".window-header",
          "body": [".window-content", {
            "form": [".form", {
              "username": ".username",
              "password": ".password",
              "submit": ".submit"
            }]
          }]
        }],
        "addProducts": [".window-add_products", {
          "header": ".window-header",
          "body": [".window-content", {
            "selectMarket": ".select-market",
            "selectBranch": ".select-branch",
            "form": [".form", {
              "name": ".name",
              "description": ".description",
              "price": ".price",
              "image": ".image-selector",
              "submit": ".submit"
            }]
          }]
        }]
      }]
    });


    // login
    // first check if works with token
    loginWithToken();

    // functions
    function userLogin() {
      loaded();
      focusWindow('login');
    }

    // function to login with username and password
    function login(username, password) {
      // trying to login with the AccountService
      AccountService.login(username, password)
        .then(function(success) {
          // if the login was a success
          if (success) {
            // login with the token again and get the information of the account
            loginWithToken(true);
          } else {
            // no success
            // output it at the console and at the screen
            console.log("failed to login");
            NotifierService.info("Anmeldung fehlgeschlagen");
          }
        });
    }

    function loginWithToken(abort) {
      // declare variables
      var status;
      // get status for loggin in
      status = AccountService.loginWithToken();
      // if failed because of there is no token
      if (status === -1) {
        // go to the login page
        userLogin();
      } else {
        status.then(function(success) {
          // check if success
          if (success) {
            // say that we logged in!!
            loggedIn();
          } else {
            // no success?
            console.log("failed to login with token");
            // if we mustn't call the function userLogin()
            if (abort) {
              // throw an error
              console.error("couldn't use token after logging in");
              NotifierService.error("Etwas scheint nicht richtig zu funktionieren: Token ging nicht");
            } else {
              // go to page login
              userLogin();
            }
          }
        })
        .catch(function () {
          // throw error that we couldn't login
          NotifierService.error("Anmeldung mit gespeicherten Anmeldedaten fehlgeschlagen!");
          // if we can go to page login
          if (!abort) {
            // go to page login
            userLogin();
          }
        })
      }
    }

    function loggedIn() {
      // feedback for client
      NotifierService.success("Angemeldet!");
      // tell variable isLoggedIn the great news: we are logged in!
      isLoggedIn = true;
      // fill all windows with content
      fillWindowAccountDetails();
      fillWindowAddProducts();
      // hide loadscreen
      loaded();
      // focus information
      focusWindow(localStorage.accountLastVisitedWindow || 'account_information');
    }

    function loaded() {
      // hide loadscreen and show the windows
      elements.loadscreen.style.display = "none";
      elements.windows.style.display = "inherit";
      // fill the login window with data and make it usable
      fillWindowLogin();
    }

    function fillWindowAccountDetails() {
      // declare variables
      var win, element, info;
      // get information of the account
      info = AccountService.getInformation();
      console.log(info);
      // get window div
      win = elements.windows.elements.accountDetails.elements.body.elements;
      // set the username
      element = win.username;
      element.innerHTML = info.account.username;
    }

    function fillWindowLogin() {
      // declare variables as local
      var element, win;
      // get window div
      win  = elements.windows.elements.login.elements.body.elements;
      // get form elements
      form = win.form.elements;
      // when pressing enter in username input focus the password input
      form.username.onkeydown = function(event) {
        if (event.keyCode == 13) {
          form.password.focus();
        }
      }
      // when pressing enter in password input focus the submit button
      form.password.onkeydown = function(event) {
        if (event.keyCode == 13) {
          form.submit.focus();
        }
      }
      // when clicking on submit button login with the username and password
      form.submit.onclick = function() {
        login(form.username.value, form.password.value);
      }
    }

    function fillWindowAddProducts() {
      // declare local variables
      var element, win, market, info, branch, e, name, description, price, form;
      // get info of account
      info = AccountService.getInformation();
      // get window div
      win = elements.windows.elements.addProducts.elements.body.elements;

      // get the market selector
      element = win.selectMarket;
      // reset the list/options
      element.innerHTML = "";
      // loop through all markets
      for (market in info.markets) {
        // get current market
        market = info.markets[market];
        // output market
        console.log("market:", market);
        // create a option/entry and add it
        e = document.createElement("OPTION");
        e.innerHTML = market.name;
        e.market = market;
        element.appendChild(e);
      }
      // refresh the branches when changing the market
      element.onchange = function() {
        if (this.options.length) {
          win.selectBranch.refresh(info.branches, this.options[this.selectedIndex].market);
        }
      }

      // get element container
      form = win.form.elements;
      // adding the product on submit
      form.submit.onclick = function() {
        // get the id of selected market and branch
        market = win.elements.select_market.options[win.elements.select_market.selectedIndex].market.id;
        branch = win.elements.select_branch.options[win.elements.select_branch.selectedIndex].branch.id;
        // output got info
        console.log(market, branch);
        // get all other info typed in the form
        name = form.name.value;
        description = form.description.value;
        price = form.price.value;
        image = form.image.files[0];
        // add/upload the product
        ProductService.addProduct(market, branch, name, description, price, image);
        // reset the form
        form.name.value = "";
        form.description.value = "";
        form.price.value = "";
      }

      // refreshing the branches
      win.selectBranch.refresh = function(branches, market) {
        console.log("refresh");
        // declaring variables as local
        var ranch, market;
        // clear the list of options/entries
        this.innerHTML = "";
        // loop through all branches
        for (branch in branches) {
          // get current branch
          branch = branches[branch];
          // if the market of the branch is the same as the current selected market
          if (branch.marketid == market.id) {
            console.log("branch:", branch);
            // add this branch as an option/entry
            e = document.createElement("OPTION");
            e.innerHTML = branch.address_place + ", " + branch.address_street + " " + branch.address_house_number;
            e.branch = branch;
            this.appendChild(e);
          }
        }
      }

      // refresh branches
      element.onchange();

    }
    

    function focusWindow(name) {
      // save last focussed window if not the login window
      if (name !== "login") {
        localStorage.accountLastVisitedWindow = name;
      }
      // check if need to login to switch windows (only can switch to login window)
      if (!isLoggedIn && name !== "login") {
        // notify user
        NotifierService.info("Du musst dich erst anmelden!");
        return;
      }
      // hide other window if there is another one
      if (focussed_window) {
        focussed_window.className = focussed_window.className.replace("window-focus", "");
      }

      // declare element as local variable
      var element;
      // get the window
      element = elements.windows.elements[name];
      // show error when the window doesn't exists
      if (!element) {
        console.error("window '"+name+"' doesn't exists");
        NotifierService.error("ausgewählte Seite existiert nicht");
        return;
      }
      // focus the window
      element.className += " window-focus";
      // update the current focussed window variable
      focussed_window = element;
    }


    // scope functions
    scope.gotoPage = function(page) {
      focusWindow(page);
    };


    return scope;
  }
]);
